states = list('AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz')
comands = [1,3,4]

commandList = [['a', 1, 'bc'],
['ad', 1, 'bc'],
['bc', 2, 'ad'],
['bc', 3, 'qt'],
['qt', 4, 'w']]

finalyState = ['w', 'c', 'a']

symbol = input('Стартовая позиция: ')
flagErrorCmd = None

for i in range(0, len(comands)):
    flagErrorCmd = True
    for cmd in commandList:
        if (comands[i] == cmd[1]) and (symbol == cmd[0]):
            symbol = cmd[2]
            print(cmd)
            flagErrorCmd = False
            break

    if flagErrorCmd:
        print('Аварийное завершение: ошибка выполнения команды!')
        break

flagErrorEndSymbol = False
#OLD
"""
for item in finalyState:
    if symbol == item:
        flagErrorEndSymbol = True
        break
"""
#NEW
for item in finalyState:
    if symbol.find(item) != -1:
        flagErrorEndSymbol = True
        break

if not flagErrorCmd:
    if flagErrorEndSymbol:
        print('Команды выполнены успешно!')
    else:
        print('Команды выполнены, но конечный результат не входит в можество резульатов!')