import copy
import itertools
commandsList = [['a', 1, 'b'],
               ['a', 1, 'c'],
               ['c', 2, 'b'],
               ['c', 2, 'a']
               ]

startState = ['a']

def getClearCommandsList(ComList):
    clearCL = []
    for i in range(0, len(ComList)):
        flagAdd = True
        for j in range(i + 1, len(ComList)):
            if ComList[i] == ComList[j]:
                flagAdd = False
        if flagAdd:
            clearCL.append(ComList[i])
    return clearCL

def getGroupedList():
    groupedList = []
    for itemCL in commandsList:
        groupedList.append(findLikeCommands(itemCL))

    return groupedList

def replace(comb, cmds):
    for i in range(0, len(cmds)):
        lenState = len(cmds[i][2])
        for item in comb:
            amtCoincidences = 0
            if lenState == len(item):
                for j in range(0, lenState):
                    if cmds[i][2].find(item[j]) != -1:
                        amtCoincidences += 1
                if amtCoincidences == lenState:
                    cmds[i][2] = item
                    amtCoincidences = 0

    return cmds


def findLikeCommands(sample):
    numbersCommands = []
    likeCommands = [sample]
    for i in range(0, len(commandsList)):
        if sample[0] == commandsList[i][0] and sample[1] == commandsList[i][1] and sample[2] != commandsList[i][2]:
            numbersCommands.append(i)
            likeCommands.append(commandsList[i])
    numbersCommands.reverse()
    for number in numbersCommands:
        commandsList.pop(number)

    return likeCommands

def firstStep(groupedList):
    detCmds = []
    for itemGL in groupedList:
        cmd = [itemGL[0][0], itemGL[0][1]]
        finalState = ""
        for state in itemGL:
            finalState += state[2]
        cmd.append(finalState)
        detCmds.append(cmd)
    return detCmds

def getOptions():
    options = []
    for item in commandsList:
        if len(options) == 0:
            options.append(item[0])
            if item[0] != item[2]:
                options.append(item[2])
        else:
            flagAdd1 = True
            flagAdd2 = True
            for itemComb in options:
                if itemComb == item[0]:
                    flagAdd1 = False
                if itemComb == item[2]:
                    flagAdd2 = False
            if flagAdd1:
                options.append(item[0])
            if flagAdd2:
                options.append(item[2])
    return options

def getAllCombinations(comb):
    amt = len(comb)
    newComb = []
    for i in range(1, amt+1):
        out = list(itertools.combinations(comb, i))
        for item in out:
            newComb.append(''.join(list(item)))

    return  newComb

def getLink(comb, detCmds):
    for itemCM in comb:
        amt = len(detCmds)
        for i in range(0, amt):
            if itemCM.find(detCmds[i][0]) != -1:
                detCmds.append([itemCM, detCmds[i][1], detCmds[i][2]])


    return detCmds

def simplify(comb, startState):
    i = 0
    while i < len(comb):
        flag = True
        try:
            startState.index(comb[i][0])
            flag = False
        except ValueError:
            for item in comb:
                if comb[i][0] == item[2]:
                    flag = False
                    break
        if flag:
            comb.pop(i)
        else:
            i += 1
    return comb





commandsList = getClearCommandsList(commandsList)
opts = getOptions()
comb = getAllCombinations(opts)
groupedList = getGroupedList()
detCmds = firstStep(groupedList)
detCmds = replace(comb, detCmds)
out = getLink(comb,detCmds)
out = getClearCommandsList(out)
out = simplify(out, opts)


print(out)

f = open('outLab4.txt', 'w')
f.write('[')
for i in range(0, len(out)):
    f.write(str(out[i]))
    if i == len(out)-1:
        f.write(']')
    else:
        f.write(',\n')
