commandList = [['a', 1, 'b'],
               ['a', 1, 'c'],
               ['c', 2, 'd'],
               ['c', 2, 'a'],
               ['c', 3, 'q'],
               ['c', 3, 't'],
               ['t', 4, 'w']
               ]
task = [1,3,4]
endsStates = ['w', 'c', 'a']
startState = input("Start state: ")

order = 0
flagExit = False
def findAvailableCommands(command, state):
    availableCommands = []
    for i in commandList:
        if i[0] == state and i[1] == command:
            availableCommands.append(i)
    return availableCommands

def checkEndState(state):
    for i in endsStates:
        if i == state:
            return True
    return False

def taskExecution(order, state):
    if order == len(task):
        if checkEndState(state):
            return True
        else:
            print('Ошибка конечного состояния, попытка вернуться к предыдущему состоянию')
            return False
    else:
        availableCommands = findAvailableCommands(task[order], state)
        if availableCommands == []:
            print('Ошибка выполнения команды, попытка вернуться к предыдущему состоянию')
            return False
        else:
            for i in availableCommands:
                print('Состояние [' + i[0] + '] команда перехода - ' + str(i[1]) + ' в состояние [' + i[2] + ']')
                result = taskExecution(order + 1, i[2])
                if result:
                    return result
            return False

if taskExecution(0, startState):
    print('Програма выполнена успешно')
else:
    print('Ошибки при выполнение программы')

