def isOperator(x):
    opertars = [['#'], ['+', '-', '*', '/']]
    type = 0
    for i in opertars:
        type += 1
        for j in i:
            if x == j:
                return type
    return False

def isVar(x):
    var = 'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz'
    for i in var:
        if x == i:
            return True
    return False

stack = []
term = input('Выражение: ')
term = list(term)
flag = True

while term != [] and flag:
    element = term.pop(0)
    if isOperator(element):
        if isOperator(element) == 1:
            stack.append([None])
        elif isOperator(element) == 2:
            stack.append([None, None])
    elif isVar(element) and stack != []:
        if len(stack[-1]) == 1:
            if stack[-1][0] == None:
                stack[-1][0] = element
            else:
                flag = False
        else:
            if stack[-1][0] == None:
                stack[-1][0] = element
            elif stack[-1][1] == None:
                stack[-1][1] = element
            else:
                flag = False
        flag2 = True
        while flag2:
            trigger = True
            for i in stack[-1]:
                if i == None:
                    trigger = False
                    flag2 = False
            if trigger:
                stack.pop()
                if stack != []:
                    for i in range(0, len(stack[-1])):
                        if stack[-1][i] == None:
                            stack[-1][i] = 'A'
                            break
                else:
                    flag2 = False
                    if term != []:
                        flag = False
                

    else:
        flag = False

if stack != []:
    flag = False



if flag:
    print('Выражение верное')
else:
    print('Выражение не верное')